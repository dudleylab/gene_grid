# Leo Skin Gene Grid

## Installation:

### 1. Python Packages

Install the required Python packages from the `requirements.txt` file located in the root folder of this repository.


    pip install -r /path/to/requirements.txt


### 2. Database Setup

Create a folder within the root folder called `instance`, and within that folder, create a file called `config.py`.

The repository should now look like:


    gene_grid/
    ├── config.py
    ├── gene_grid
    │   ├── build_gene_dict.py
    │   ├── build_gene_dict.pyc
    │   ├── forms.py
    │   ├── forms.pyc
    │   ├── __init__.py
    │   ├── __init__.pyc
    │   ├── nav.py
    │   ├── nav.pyc
    │   ├── templates
    │   │   ├── base.html
    │   │   └── grid.html
    │   └── visualizations.py
    ├── instance
    │   └── config.py
    ├── README.md
    ├── requirements.txt
    └── runserver.py


The `gene_grid/instance/config.py` file holds configuration information that is instance specific, and often private information that shouldn't be included in the git repository. In this project, the information that needs to be in this file is your MySQL database information.

Add this information to `gene_grid/instance/config.py` as follows:


    DB_HOST = <your MySQL server host address>
    DB_PORT = <your MySQL server port (default 3306)>
    DB_USER = <your MySQL server username>
    DB_PASSWORD = <your MySQL server password>
    DB = <the database you are trying to access on the MySQL server>


### 3. Other configuration options (optional)

Other configuration options are listed in `gene_grid/config.py` (not to be confused with `gene_grid/instance/config.py`).

These options are as follows (parts taken from the flask documentation)

#### DEBUG

Setting DEBUG = True will enable the debugging features of flask, which includes but is not limited to console logging and an in-browser debugger if the application hits an error.

Default: `False`

#### USE_CSRF

Tells Gene Grid whether or not to use cross-site-request-forgery protection. If enabled, this is used on the form to select which indication you want to view (Psoriasis or Atopic Dermatitis).

Default: `False`

####  SECRET_KEY

Gene Grid uses sessions, and therefore flask requries that a secret key is set. This allows Gene Grid to securly store session variables. Additionally, if `USE_CSRF` is set to `True`, the secret key is used to for cross-site-request-forgery protection (see `USE_CSRF`).

Set this to a complex random value when you want to use the secure cookie for instance.

Default: `'secret_key'`

#### USE_CACHED_GENE_DICT

By default, Gene Grid will query the database upon startup. For development purposes you may want to cache the query results so server startup is faster. Setting `USE_CACHED_GENE_DICT` to `True` will tell Gene Grid to use this cache. However, the cache must first be created. In order to do this, run

    python /path/to/build_gene_dict.py


By default, `build_gene_dict.py` is located at `gene_grid/gene_grid/build_gene_dict.py`. Once the above command is run, you will be prompted to enter your database information so that the query can be made. This will generate a file called `dict.txt` in `gene_grid/gene_grid`. Once this is done, you may set `USE_CACHED_GENE_DICT` to `True`

Default: `False`


## Usage

To start the server, simply run

    python /path/to/runserver.py

By default, `runserver.py` is located in the root folder of the repository (i.e. `gene_grid/runserver.py`).
