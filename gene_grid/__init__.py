from flask import Flask
from flask_bootstrap import Bootstrap
from gene_grid.filters import *

app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config')
app.config.from_pyfile('config.py')
app.jinja_env.filters['entrezID'] = entrezID
app.jinja_env.filters['genecards'] = genecards

from gene_grid.nav import nav
from gene_grid.visualizations import visualizations


Bootstrap(app)

nav.init_app(app)

app.register_blueprint(visualizations)

