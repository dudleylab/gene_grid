import MySQLdb
import MySQLdb.cursors
from flask import Blueprint, render_template, request, session
from flask_bootstrap import __version__ as FLASK_BOOTSTRAP_VERSION
from flask_nav.elements import Navbar
import pandas as pd

from gene_grid import app
import gene_grid.filters
from gene_grid.forms import IndicationForm
from gene_grid.nav import nav
from gene_grid.queries import *

from build_gene_dict import build_dictionary


pd.set_option('display.max_colwidth', -1)

DB_CREDS = {
    'host': app.config['DB_HOST'],
    'port': app.config['DB_PORT'],
    'user': app.config['DB_USER'],
    'passwd': app.config['DB_PASSWORD'],
    'db': app.config['DB'],
    'cursorclass': MySQLdb.cursors.DictCursor
}

visualizations = Blueprint('visualizations', __name__)
# If USE_CACHED_GENE_DICT is set, build_dictionary will load the gene
# dictionary from the dict.txt file instead of querying it. See the
# README for further information.
data = build_dictionary(app.config['USE_CACHED_GENE_DICT'], DB_CREDS)


# Currently doesn't have any views, but is here to allow for more views
# as the application grows.
@nav.navigation()
def side():
    return Navbar('Skin KB')


# The main route (/) points to the gene grid. If the application ever
# grows to have multiple views, it may or may not make sense to change
# this route.
@visualizations.route('/', methods=['GET', 'POST'])
def grid():
    form = IndicationForm(csrf_enabled=app.config['USE_CSRF'])
    indication = form.indication.data
    session["indication"] = indication
    return render_template('grid.html',
                           data=data[indication],
                           indication=indication,
                           form=form)


# The AJAX endpoint to fetch data for the detail tables. Note that the
# endpoint actually returns HTML for the table, not just raw data.
@visualizations.route('/source_table', methods=['POST'])
def source_table():
    table = request.form["source_table"]
    indication = session["indication"]
    entrezid = request.form["entrezid"]
    if table == "omni_gwasdb_skin":
        spelled_indication = ('Psoriasis' if indication == 'PSO'
                              else 'Atopic dermatitis')
        queries = [gwasdb_skin(spelled_indication, entrezid)]
    elif table == "omni_meta_analysis":
        queries = [meta_analysis(indication, entrezid)]
    elif table == "drug_targets":
        queries = [sea_db_targets(entrezid), pubmed_targets(entrezid)]
    elif table == "omni_pubmed_nlp":
        # PSO: D011565
        # AD: D003876
        MESH = ("D011565" if indication == "PSO"
                else "D003876")
        table_id = request.form["table_id"]
        queries = [pubmed_nlp(MESH, table_id)]
    # If there is no table specific logic, this will perform a generic
    # query (ie "SELECT * FROM <table> WHERE entrezgeneid = <entrezid>").
    # This query is currently used for the eQTLs and coexp tables.
    else:
        queries = [generic(table, entrezid)]

    conn = MySQLdb.connect(**DB_CREDS)

    # Because the drug targets query involves stacking two tables, we
    # always set queries to a list and concatenate their resulting
    # Pandas DataFrames to comply with DRY and more easily allow for
    # more multi-query tables going forward. Note the concatenating a
    # single DataFrame will just result in that DataFrame.
    df = pd.concat(pd.read_sql(query, conn) for query in queries)
    html_table = df.to_html(index=None)
    return html_table
