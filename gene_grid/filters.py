"""Custom Jinja2 filters."""


def entrezID(ID):
    """Return a link to NCBI with the given entrez ID."""
    return "http://www.ncbi.nlm.nih.gov/gene/" + ID


def genecards(symbol):
    """Return a link to Genecards with the given gene symbol."""
    return "http://www.genecards.org/cgi-bin/carddisp.pl?gene=" + symbol

