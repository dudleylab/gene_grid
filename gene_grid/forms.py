"""All the forms for the flask application."""
from flask_wtf import Form
from wtforms import SelectField, SubmitField, IntegerField


class GenesBargraphForm(Form):
    """Form class for form for getting a bar graph of genes."""

    indication = SelectField('Indication',
                             choices=[('All',) * 2,
                                      ('PSO', 'Psoriasis'),
                                      ('Atopic dermatitis',) * 2,
                                      ('Acne (severe)',) * 2],
                             default='All')

    chemical = SelectField('Chemical',
                           choices=[('All',) * 2,
                                    ('vorinostat', 'Vorinostat'),
                                    ('doxylamine', 'Doxylamine')],
                           default='All')

    genes = IntegerField('Number of Genes', default=20)
    submit = SubmitField('Visualize')


class IndicationForm(Form):
    """Form class for form to select what indication to view in the grid."""

    indication = SelectField('Indication',
                             choices=[('PSO', 'Psoriasis'),
                                      ('AD', 'Atopic Dermatitis')],
                             default='PSO')

    submit = SubmitField('Load Table')
