"""Module to contain queries to better organize logic."""


def gwasdb_skin(indication, entrezid):
    """Query to get data from omni_gwasdb_skin."""
    return """SELECT *
              FROM omni_gwasdb_skin gs
                LEFT JOIN omni_hgnc hg
                ON gs.genesymbol = hg.genesymbol
              WHERE gwas_trait = '{}'
                AND entrezgeneid = '{}'
              """.format(indication, entrezid)


def meta_analysis(indication, entrezid):
    """Query to get data from omni_meta_analysis."""
    return """SELECT *
              FROM omni_meta_analysis
              WHERE indication='{}'
                AND entrezgeneid='{}';
              """.format(indication, entrezid)


def generic(table, entrezid):
    """Query to get data from a generic table."""
    return """SELECT *
              FROM {}
              WHERE entrezgeneid='{}'
              """.format(table, entrezid)


def sea_db_targets(entrezid):
    """Query to get data for drug targets from sea and DB."""
    return """SELECT DBID,
                     feature,
                     feature_type,
                     organism,
                     entrezgeneid,
                     genesymbol
              FROM omni
              WHERE entrezgeneid = '{}'
                AND omni.`source` in ('omni_sea_targets',
                                      'omni_drugbank_targets');
              """.format(entrezid)


def pubmed_targets(entrezid):
    """Query to get data for drug targets from pubmed_nlp."""
    return """SELECT id2.name as feature,
                     NULL as feature_type,
                     id1.dbid as entrezgeneid,
                     hg.genesymbol,
                     CASE id2.species
                       WHEN '' THEN 'Human'
                       ELSE id2.species
                     END as organism,
                     CASE id2.dbid_type
                       WHEN 'CHEBI' THEN CONCAT('CHEBI:', id2.dbid)
                       ELSE id2.dbid
                     END as DBID
              FROM omni_pubmed_nlp.omni_pubmed_id_info as id1
                INNER JOIN
                   omni_skinkb.omni_hgnc as hg on id1.dbid = hg.entrezgeneid
                INNER JOIN
                   omni_pubmed_nlp.omni_pubmed_pairs as p
                ON p.table_id1 = id1.table_id
                INNER JOIN
                   omni_pubmed_nlp.omni_pubmed_id_info as id2
                ON p.table_id2 = id2.table_id
              WHERE id1.dbid = '{}'
                AND id2.type = 'Chemical';
              """.format(entrezid)


def pubmed_nlp(MESH, table_id):
    """Query to get data from pubmed_nlp."""
    return """SELECT pmm1.pmid,
                     pd1.title,
                     pd1.journal,
                     pd1.submission_date,
                     pmm1.tf_idf +
                       pmm2.tf_idf +
                       UNIX_TIMESTAMP(pd1.submission_date) / 31536000
                     as score
              FROM pubmed_nlp.id_to_dbid_map as dbm,
                   pubmed_nlp.id_to_pmid_map as pmm1
                LEFT JOIN
                   pubmed_nlp.pmid_data as pd1
                ON pmm1.pmid = pd1.pmid,
                   pubmed_nlp.id_to_pmid_map as pmm2
                LEFT JOIN
                   pubmed_nlp.pmid_data as pd2
                ON pmm2.pmid = pd2.pmid
              WHERE dbm.dbid = '{}'
                AND dbm.rank = 1
                AND pmm1.id = dbm.id
                AND pmm2.id = {}
                AND pmm1.pmid = pd1.pmid
                AND pd1.submission_date > 2000-01-01
                AND pmm1.pmid = pmm2.pmid
              GROUP BY pmid
              ORDER by score DESC;
              """.format(MESH, table_id)
