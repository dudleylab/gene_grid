"""Writes a dictionary to dict.txt that contains indications for keys.

Values are multilayer dictionaries that each describe one gene. The
first-level key in each of these subdictionaries is the gene's entrez
id.
"""
import os.path

import MySQLdb
import sys
import MySQLdb.cursors
# from gene_grid import app

def pull_data(DB_CREDS):
    conn = MySQLdb.connect(**DB_CREDS)
    cur = conn.cursor()

    sql = """
                  SELECT
                      omni.entrezgeneid,
                      genesymbol,
                      omni.indication,
                      `source`,
                      grps,
                      table_id,
                      MIN(val),
                      fisher_p
                  FROM
                      omni
                  WHERE
                      omni.entrezgeneid IS NOT NULL
                          AND omni.entrezgeneid != "None"
                          AND val_type IN ('p_value' , 'k_me')
                          AND omni.indication = '{}'
                          AND fisher_p < 0.00000001
                  GROUP BY omni.entrezgeneid , genesymbol , omni.indication , `source` , grps, table_id
                  ;
                  """

    pso_sql = sql.format('PSO')
    ad_sql = sql.format('AD')

    cur.execute(pso_sql)


    print "Query Complete"

    pso_data = cur.fetchall()

    cur.execute(ad_sql)

    print "Query Complete"

    ad_data = cur.fetchall()

    conn.close()

    return (pso_data, ad_data)

def build_dictionary(USE_CACHED_GENE_DICT, DB_CREDS):

    GENE_DICT_FILE =  os.path.join(os.path.dirname(__file__), 'dict.txt')
    if USE_CACHED_GENE_DICT:
        return eval(open('/home/ericmarkmartin/Development/gene_grid/gene_grid/dict.txt', 'r').read())

    gene_dict = {
        'PSO': {},
        'AD': {},
    }

    pso_data, ad_data = pull_data(DB_CREDS)

    for indication, data in [('PSO', pso_data), ('AD', ad_data)]:
        for row in data:
            (entrezid,
             genesymbol,
             indication,
             source,
             grps,
             table_id,
             val,
             fisher_score) = (row['entrezgeneid'],
                              row['genesymbol'],
                              row['indication'],
                              row['source'],
                              row['grps'],
                              row['table_id'],
                              row['MIN(val)'],
                              row['fisher_p'])

            if entrezid not in gene_dict[indication]:
                gene_dict[indication][entrezid] = {
                    # 'entrezid': entrezid,
                    'genesymbol': genesymbol,
                    'omni_coexp': {
                        None: {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        }
                    },
                    'omni_deg': {
                        'LSvsCTRL': {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        },
                        'LSvsNL': {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        },
                        'NLvsCTRL': {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        }
                    },
                    'omni_eqtls': {
                        'fat_p': {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        },
                        'lcl_p': {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        },
                        'skin_p': {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        }
                    },
                    'omni_gwasdb_skin': {
                        None: {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        }
                    },
                    'omni_meta_analysis': {
                        'LSvsCTRL': {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                    },
                        'LSvsNL': {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        },
                        'NLvsCTRL': {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        }
                    },
                    'omni_pubmed_nlp': {
                        None: {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        }
                    },
                    'omni_sea_targets': {
                        None: {
                            'p_value': '',
                            'table_id': '',
                            'table_ids': [][:]
                        }
                    },
                    'evidence_count': 0,
                    'fisher_p': fisher_score
                }

            if val is not None:
                tmpVal = gene_dict[indication][entrezid][source][grps]['p_value']
                if val < tmpVal or tmpVal == '':
                    gene_dict[indication][entrezid][source][grps]['p_value'] = val
                    gene_dict[indication][entrezid][source][grps]['table_id'] = table_id

    return gene_dict

if __name__ == "__main__":
    from getpass import getpass

    host = raw_input('DB host: ')
    port = input('DB port: ')
    user = raw_input('DB username: ')
    passwd = getpass(prompt='DB password: ')
    db = raw_input('Database: ')

    DB_CREDS = {
        'host': host,
        'port': port,
        'user': user,
        'passwd': passwd,
        'db': db,
        'cursorclass': MySQLdb.cursors.DictCursor
    }

    print "Building Dictionary"
    gd = build_dictionary(False, DB_CREDS)
    print "Done building dictionary"
    print "Writing dictionary"
    target = open('dict.txt', 'w')
    target.write(str(gd))
    print "Done writing dictionary"
